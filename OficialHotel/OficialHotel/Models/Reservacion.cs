namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reservacion")]
    public partial class Reservacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Reservacion()
        {
            DetalleFactura = new HashSet<DetalleFactura>();
        }

        public int ReservacionId { get; set; }

        public DateTime Ckeck_in { get; set; }

        public DateTime Ckeck_out { get; set; }

        public int UsuarioId { get; set; }

        public int HabitacionId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalleFactura> DetalleFactura { get; set; }

        public virtual Habitacion Habitacion { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
