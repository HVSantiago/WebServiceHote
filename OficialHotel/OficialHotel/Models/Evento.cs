namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Evento")]
    public partial class Evento
    {
        public int EventoId { get; set; }

        [Required]
        [StringLength(50)]
        public string NombreEvento { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }

        public int CategoriaEventoId { get; set; }

        public int HotelId { get; set; }

        public virtual CategoriaEvento CategoriaEvento { get; set; }

        public virtual Hotel Hotel { get; set; }
    }
}
