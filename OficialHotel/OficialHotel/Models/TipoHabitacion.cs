namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoHabitacion")]
    public partial class TipoHabitacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoHabitacion()
        {
            Habitacion = new HashSet<Habitacion>();
        }

        public int TipoHabitacionId { get; set; }

        [Required]
        [StringLength(50)]
        public string NombreTipoHabitacion { get; set; }

        public decimal Precio { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Habitacion> Habitacion { get; set; }
    }
}
