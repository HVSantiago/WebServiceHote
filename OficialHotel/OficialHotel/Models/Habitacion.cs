namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Habitacion")]
    public partial class Habitacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Habitacion()
        {
            Reservacion = new HashSet<Reservacion>();
        }

        public int HabitacionId { get; set; }

        public int TipoHabitacionId { get; set; }

        public int HotelId { get; set; }

        public int EstadoHabitacionId { get; set; }

        public virtual EstadoHabitacion EstadoHabitacion { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual TipoHabitacion TipoHabitacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reservacion> Reservacion { get; set; }
    }
}
