namespace OficialHotel.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DB_Entities : DbContext
    {
        public DB_Entities()
            : base("name=DB_Entities")
        {
        }

        public virtual DbSet<AdmonApp> AdmonsApp { get; set; }
        public virtual DbSet<AdmonHotel> AdmonsHotel { get; set; }
        public virtual DbSet<CategoriaEvento> CategoriasEvento { get; set; }
        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<DetalleFactura> DetallesFactura { get; set; }
        public virtual DbSet<EstadoHabitacion> EstadosHabitacion { get; set; }
        public virtual DbSet<Evento> Eventos { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<Habitacion> Habitaciones { get; set; }
        public virtual DbSet<Hotel> Hoteles { get; set; }
        public virtual DbSet<Municipio> Municipios { get; set; }
        public virtual DbSet<Reservacion> Reservaciones { get; set; }
        public virtual DbSet<Servicio> Servicios { get; set; }
        public virtual DbSet<TipoHabitacion> TipoHabitacion { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<MayorSolicitud> MayorSolicitud { get; set; }
        public virtual DbSet<Reservaciones> ReservacionesR { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdmonApp>()
                .Property(e => e.NombreAdmonApp)
                .IsUnicode(false);

            modelBuilder.Entity<AdmonApp>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<AdmonApp>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<AdmonHotel>()
                .Property(e => e.NombreAdmonHotel)
                .IsUnicode(false);

            modelBuilder.Entity<AdmonHotel>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<AdmonHotel>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<CategoriaEvento>()
                .Property(e => e.NombreCategoriaEvento)
                .IsUnicode(false);

            modelBuilder.Entity<CategoriaEvento>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.CategoriaEvento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Departamento>()
                .Property(e => e.NombreDepartamento)
                .IsUnicode(false);

            modelBuilder.Entity<Departamento>()
                .HasMany(e => e.Hotel)
                .WithRequired(e => e.Departamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EstadoHabitacion>()
                .Property(e => e.NombreEstadoHabitacion)
                .IsUnicode(false);

            modelBuilder.Entity<EstadoHabitacion>()
                .HasMany(e => e.Habitacion)
                .WithRequired(e => e.EstadoHabitacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Evento>()
                .Property(e => e.NombreEvento)
                .IsUnicode(false);

            modelBuilder.Entity<Evento>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .HasMany(e => e.DetalleFactura)
                .WithRequired(e => e.Factura)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Habitacion>()
                .HasMany(e => e.Reservacion)
                .WithRequired(e => e.Habitacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .Property(e => e.NombreHotel)
                .IsUnicode(false);

            modelBuilder.Entity<Hotel>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.AdmonHotel)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.Habitacion)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.Servicio)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Municipio>()
                .Property(e => e.NombreMunicipio)
                .IsUnicode(false);

            modelBuilder.Entity<Municipio>()
                .HasMany(e => e.Hotel)
                .WithRequired(e => e.Municipio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reservacion>()
                .HasMany(e => e.DetalleFactura)
                .WithRequired(e => e.Reservacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Servicio>()
                .Property(e => e.NombreServicio)
                .IsUnicode(false);

            modelBuilder.Entity<Servicio>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Servicio>()
                .Property(e => e.Precio)
                .HasPrecision(5, 2);

            modelBuilder.Entity<TipoHabitacion>()
                .Property(e => e.NombreTipoHabitacion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoHabitacion>()
                .Property(e => e.Precio)
                .HasPrecision(5, 2);

            modelBuilder.Entity<TipoHabitacion>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoHabitacion>()
                .HasMany(e => e.Habitacion)
                .WithRequired(e => e.TipoHabitacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.NombreUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.ApellidoUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Factura)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Reservacion)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MayorSolicitud>()
                .Property(e => e.NombreHotel)
                .IsUnicode(false);

            modelBuilder.Entity<Reservaciones>()
                .Property(e => e.NombreHotel)
                .IsUnicode(false);

            modelBuilder.Entity<Reservaciones>()
                .Property(e => e.correo)
                .IsUnicode(false);
        }
    }
}
