namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Reservaciones
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string NombreHotel { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ReservacionId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime Ckeck_in { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime Ckeck_out { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string correo { get; set; }
    }
}
