namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Servicio")]
    public partial class Servicio
    {
        public int ServicioId { get; set; }

        [Required]
        [StringLength(50)]
        public string NombreServicio { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        public decimal Precio { get; set; }

        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }
    }
}
