namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MayorSolicitud")]
    public partial class MayorSolicitud
    {
        public int? Cantidad_Reservas { get; set; }

        [Key]
        [StringLength(50)]
        public string NombreHotel { get; set; }
    }
}
