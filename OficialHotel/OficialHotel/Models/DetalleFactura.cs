namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleFactura")]
    public partial class DetalleFactura
    {
        public int DetalleFacturaId { get; set; }

        public int FacturaId { get; set; }

        public int ReservacionId { get; set; }

        public virtual Factura Factura { get; set; }

        public virtual Reservacion Reservacion { get; set; }
    }
}
