namespace OficialHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdmonApp")]
    public partial class AdmonApp
    {
        public int AdmonAppId { get; set; }

        [Required]
        [StringLength(50)]
        public string NombreAdmonApp { get; set; }

        [Required]
        [StringLength(50)]
        public string Correo { get; set; }

        [Required]
        [StringLength(10)]
        public string Password { get; set; }
    }
}
