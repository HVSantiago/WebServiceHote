﻿using OficialHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OficialHotel.Controllers
{
    public class HomeController : Controller
    {
        private DB_Entities db = new DB_Entities();
        public ActionResult Login()
        {
            ViewBag.Title = "Home Page";

            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UsuarioLogIn objUser)
        {
            if (ModelState.IsValid)
            {
                var obj = db.Usuarios.Where(a => a.Correo.Equals(objUser.Correo) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                
                if (obj != null)
                {
                    System.Diagnostics.Debug.WriteLine("Estoy en Usuario");
                    Session["UserID"] = obj.UsuarioId.ToString();
                    Session["Correo"] = obj.Correo.ToString();
                    return RedirectToAction("UserDashBoard");

                }
                else
                {
                    var adminApp = db.AdmonsApp.Where(a => a.Correo.Equals(objUser.Correo) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                    if(adminApp != null)
                    {
                        System.Diagnostics.Debug.WriteLine("Estoy en AdminApp");
                        Session["UserID"] = adminApp.AdmonAppId.ToString();
                        Session["Correo"] = adminApp.Correo.ToString();
                        return RedirectToAction("AdmonApp");
                    }
                    else
                    {
                        var adminHotel = db.AdmonsHotel.Where(a => a.Correo.Equals(objUser.Correo) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                        if(adminHotel != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Estoy en AdminHotel");
                            Session["UserID"] = adminHotel.AdmonHotelId.ToString();
                            Session["Correo"] = adminHotel.Correo.ToString();
                            return RedirectToAction("AdmonHotel");
                        }
                    }

                }
            }
            return View(objUser);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }

        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult AdmonApp()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public ActionResult AdmonHotel()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }




    }
}
