﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using System.IO;
using OficialHotel.Models;


namespace OficialHotel.Controllers
{
    public class ReporteController : Controller
    {
        // GET: Reporte
        public ActionResult Reporte()
        {
            DB_Entities db = new DB_Entities();
            var c = (from b in db.ReservacionesR select b).ToList();
            
            ReporteReservas rpt = new ReporteReservas();
            
            rpt.Load();
            rpt.SetDataSource(c);
            Stream s = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);


            return File(s, "application/pdf");
        
        }

        

    }
}