﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OficialHotel.Models;

namespace OficialHotel.Controllers.root
{
    public class AdmonHotelController : Controller
    {
        private DB_Entities db = new DB_Entities();

        // GET: AdmonHotel
        public async Task<ActionResult> Index()
        {
            var admonsHotel = db.AdmonsHotel.Include(a => a.Hotel);
            return View(await admonsHotel.ToListAsync());
        }

        // GET: AdmonHotel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdmonHotel admonHotel = await db.AdmonsHotel.FindAsync(id);
            if (admonHotel == null)
            {
                return HttpNotFound();
            }
            return View(admonHotel);
        }

        // GET: AdmonHotel/Create
        public ActionResult Create()
        {
            ViewBag.HotelId = new SelectList(db.Hoteles, "HotelId", "NombreHotel");
            return View();
        }

        // POST: AdmonHotel/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AdmonHotelId,NombreAdmonHotel,Correo,Password,HotelId")] AdmonHotel admonHotel)
        {
            if (ModelState.IsValid)
            {
                db.AdmonsHotel.Add(admonHotel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HotelId = new SelectList(db.Hoteles, "HotelId", "NombreHotel", admonHotel.HotelId);
            return View(admonHotel);
        }

        // GET: AdmonHotel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdmonHotel admonHotel = await db.AdmonsHotel.FindAsync(id);
            if (admonHotel == null)
            {
                return HttpNotFound();
            }
            ViewBag.HotelId = new SelectList(db.Hoteles, "HotelId", "NombreHotel", admonHotel.HotelId);
            return View(admonHotel);
        }

        // POST: AdmonHotel/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AdmonHotelId,NombreAdmonHotel,Correo,Password,HotelId")] AdmonHotel admonHotel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(admonHotel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HotelId = new SelectList(db.Hoteles, "HotelId", "NombreHotel", admonHotel.HotelId);
            return View(admonHotel);
        }

        // GET: AdmonHotel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdmonHotel admonHotel = await db.AdmonsHotel.FindAsync(id);
            if (admonHotel == null)
            {
                return HttpNotFound();
            }
            return View(admonHotel);
        }

        // POST: AdmonHotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AdmonHotel admonHotel = await db.AdmonsHotel.FindAsync(id);
            db.AdmonsHotel.Remove(admonHotel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
