﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OficialHotel.Models;

namespace OficialHotel.Controllers
{
    public class HotelController : Controller
    {
        private DB_Entities db = new DB_Entities();

        // GET: Hotel
        public async Task<ActionResult> Index()
        {
            var hoteles = db.Hoteles.Include(h => h.Departamento).Include(h => h.Municipio);
            return View(await hoteles.ToListAsync());
        }

        // GET: Hotel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = await db.Hoteles.FindAsync(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // GET: Hotel/Create
        public ActionResult Create()
        {
            ViewBag.DepartamentoId = new SelectList(db.Departamentos, "DepartamentoId", "NombreDepartamento");
            ViewBag.MunicipioId = new SelectList(db.Municipios, "MunicipioId", "NombreMunicipio");
            return View();
        }

        // POST: Hotel/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "HotelId,NombreHotel,Direccion,DepartamentoId,MunicipioId")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Hoteles.Add(hotel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DepartamentoId = new SelectList(db.Departamentos, "DepartamentoId", "NombreDepartamento", hotel.DepartamentoId);
            ViewBag.MunicipioId = new SelectList(db.Municipios, "MunicipioId", "NombreMunicipio", hotel.MunicipioId);
            return View(hotel);
        }

        // GET: Hotel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = await db.Hoteles.FindAsync(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoId = new SelectList(db.Departamentos, "DepartamentoId", "NombreDepartamento", hotel.DepartamentoId);
            ViewBag.MunicipioId = new SelectList(db.Municipios, "MunicipioId", "NombreMunicipio", hotel.MunicipioId);
            return View(hotel);
        }

        // POST: Hotel/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HotelId,NombreHotel,Direccion,DepartamentoId,MunicipioId")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hotel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoId = new SelectList(db.Departamentos, "DepartamentoId", "NombreDepartamento", hotel.DepartamentoId);
            ViewBag.MunicipioId = new SelectList(db.Municipios, "MunicipioId", "NombreMunicipio", hotel.MunicipioId);
            return View(hotel);
        }

        // GET: Hotel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = await db.Hoteles.FindAsync(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // POST: Hotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Hotel hotel = await db.Hoteles.FindAsync(id);
            db.Hoteles.Remove(hotel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
