﻿var ModelUsuario = function () {
    var instancia = this;
    var usuarioUri = '/api/Usuarios/';
    instancia.usuarios = ko.observableArray();
    instancia.error = ko.observable();
    instancia.usuarioCargado = ko.observable();

    //Enlasar campos con la vista
    instancia.usuarioNuevo = {
        NombreUsuario: ko.observable(),
        ApellidoUsuario: ko.observable(),
        Correo: ko.observable(),
        Password: ko.observable(),
        Nit: ko.observable()
    }
    //obtener todos los registros de la tabla
    //Obtener todos los registros de la tabla
    instancia.getAllUsuarios = function () {
        ajaxHelper(usuarioUri, 'GET').done(function (data) {
            instancia.usuarios(data);
        });
    }

    instancia.agregar = function () {
        var usuario = {
            NombreUsuario: instancia.usuarioNuevo.NombreUsuario(),
            ApellidoUsuario: instancia.usuarioNuevo.ApellidoUsuario(),
            Correo: instancia.usuarioNuevo.Correo(),
            Password: instancia.usuarioNuevo.Password(),
            Nit: instancia.usuarioNuevo.Nit()
        }
        ajaxHelper(usuarioUri, 'POST', usuario).done(function (data) {
            instancia.usuarios.push(data);
            //Tambien podemos hacer
            //intancia.getAllusuarios();
            $("#modalAgregarUsuario").modal('hide');
        });
    }

    /*4*/ instancia.editar = function () {
        var usuario = {
            UsuarioId: instancia.usuarioCargado().UsuarioId,
            NombreUsuario: instancia.usuarioCargado().NombreUsuario,
            ApellidoUsuario: instancia.usuarioCargado().ApellidoUsuario,
            Correo: instancia.usuarioCargado().Correo,
            Password: instancia.usuarioCargado().Password,
            Nit: instancia.usuarioCargado().Nit
        }
        var uriEditar = usuarioUri + usuario.UsuarioId;
        ajaxHelper(uriEditar, 'PUT', usuario)
        .done(function (data) {
            instancia.getAllUsuarios();
        });
    }

    /*5*/ instancia.eliminar = function (item) {
        var id = item.UsuarioId;
        var uri = usuarioUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllUsuarios();
        });
    }

    /*6*/ instancia.cargar = function (item) {
        instancia.usuarioCargado(item);
    }
    //Metodo para hacer peticiones
    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            //PUT, DELETE, etc.
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }
    instancia.getAllUsuarios();
}

$(document).ready(function () {
    var modelUsuario = new ModelUsuario();
    ko.applyBindings(modelUsuario);
});
