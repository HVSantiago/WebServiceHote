CREATE DATABASE HotelWeb
	USE HotelWeb


	CREATE TABLE Departamento(
	DepartamentoId INT IDENTITY(1,1) NOT NULL,
	NombreDepartamento VARCHAR(30) NOT NULL,
	PRIMARY KEY(DepartamentoId)
	);

	CREATE TABLE Municipio(
	MunicipioId INT IDENTITY(1,1) NOT NULL,
	NombreMunicipio VARCHAR(50) NOT NULL,
	PRIMARY KEY(MunicipioId)
	)

	CREATE TABLE Hotel(
	HotelId INT IDENTITY(1,1) NOT NULL,
	NombreHotel VARCHAR(50) NOT NULL,
	Direccion VARCHAR(50) NOT NULL,
	DepartamentoId INT NOT NULL,
	MunicipioId  INT NOT NULL,
	PRIMARY KEY (HotelId),
	FOREIGN KEY(DepartamentoId) REFERENCES Departamento(DepartamentoId),
	FOREIGN KEY(MunicipioId) REFERENCES Municipio(MunicipioId)
	);

	CREATE TABLE Servicio(
	ServicioId INT IDENTITY(1,1) NOT NULL,
	NombreServicio VARCHAR(50) NOT NULL,
	Descripcion TEXT NULL,
	Precio DECIMAL(5,2) NOT NULL,
	HotelId INT NOT NULL,
	PRIMARY KEY(ServicioId),
	FOREIGN KEY(HotelId) REFERENCES Hotel(HotelId)
	);

	

	CREATE TABLE CategoriaEvento(
	CategoriaEventoId INT IDENTITY(1,1) NOT NULL,
	NombreCategoriaEvento VARCHAR(50) NOT NULL,
	PRIMARY KEY(CategoriaEventoId)
	);


	CREATE TABLE Evento(
	EventoId INT IDENTITY (1,1) NOT NULL,
	NombreEvento VARCHAR(50) NOT NULL,
	Descripcion TEXT NULL,
	Fecha DATETIME NOT NULL,
	CategoriaEventoId INT NOT NULL,
	HotelId INT NOT NULL,
	PRIMARY KEY(EventoId),
	FOREIGN KEY(CategoriaEventoId) REFERENCES CategoriaEvento(CategoriaEventoId),
	FOREIGN KEY(HotelId) REFERENCES Hotel(HotelId)
	);

	
	CREATE TABLE AdmonApp(
	AdmonAppId INT IDENTITY(1,1) NOT NULL,
	NombreAdmonApp VARCHAR(50) NOT NULL,
	Correo VARCHAR(50) NOT NULL,
	Password VARCHAR(10) NOT NULL,
	PRIMARY KEY(AdmonAppId)
	);


	CREATE TABLE AdmonHotel(
	AdmonHotelId INT IDENTITY(1,1) NOT NULL,
	NombreAdmonHotel VARCHAR(50) NOT NULL,
	Correo VARCHAR(50) NOT NULL,
	Password VARCHAR(10) NOT NULL,
	HotelId INT NOT NULL,
	PRIMARY KEY(AdmonHotelId),
	FOREIGN KEY(HotelId) REFERENCES Hotel(HotelId)
	);



	CREATE TABLE Usuario(
	UsuarioId INT IDENTITY(1,1) NOT NULL,
	NombreUsuario VARCHAR(60) NOT NULL,
	ApellidoUsuario VARCHAR(60) NOT NULL,
	Correo VARCHAR(50) NOT NULL,
	Password VARCHAR(50) NOT NULL,
	Nit INT NULL,
	PRIMARY KEY(UsuarioId)
	);


	CREATE TABLE EstadoHabitacion(
	EstadoHabitacionId INT IDENTITY(1,1) NOT NULL,
	NombreEstadoHabitacion VARCHAR(50) NOT NULL,
	PRIMARY KEY(EstadoHabitacionId)
	);

	

	CREATE TABLE TipoHabitacion(
	TipoHabitacionId INT IDENTITY(1,1) NOT NULL,
	NombreTipoHabitacion VARCHAR(50) NOT NULL,
	Precio DECIMAL(5,2) NOT NULL,
	Descripcion TEXT NULL,
	PRIMARY KEY(TipoHabitacionId)
	);

	CREATE TABLE Habitacion(
	HabitacionId INT IDENTITY(1,1) NOT NULL,
	TipoHabitacionId INT NOT NULL,
	HotelId INT NOT NULL,
	EstadoHabitacionId INT NOT NULL,
	PRIMARY KEY(HabitacionId),
	FOREIGN KEY(TipoHabitacionId) REFERENCES TipoHabitacion(TipoHabitacionId),
	FOREIGN KEY(HotelId) REFERENCES Hotel(HotelId),
	FOREIGN KEY(EstadoHabitacionId) REFERENCES EstadoHabitacion(EstadoHabitacionId)
	);


	CREATE TABLE Reservacion(
	ReservacionId INT IDENTITY(1,1) NOT NULL,
	Ckeck_in DATETIME NOT NULL,
	Ckeck_out DATETIME NOT NULL, 
	UsuarioId INT NOT NULL,
	HabitacionId INT NOT NULL,
	PRIMARY KEY(ReservacionId),
	FOREIGN KEY(UsuarioId) REFERENCES Usuario(UsuarioId),
	FOREIGN KEY(HabitacionId) REFERENCES Habitacion(HabitacionId)
	);


	CREATE TABLE Factura(
	FacturaId INT IDENTITY(1,1) NOT NULL,
	UsuarioId INT NOT NULL,
	fechaFacturacion DATETIME,
	PRIMARY KEY(FacturaId),
	FOREIGN KEY(UsuarioId) REFERENCES Usuario(UsuarioId)
	);

	CREATE TABLE DetalleFactura(
	DetalleFacturaId INT IDENTITY(1,1) NOT NULL,
	FacturaId INT NOT NULL,
	ReservacionId INT NOT NULL,
	PRIMARY KEY(DetalleFacturaId),
	FOREIGN KEY(FacturaId) REFERENCES Factura(FacturaId),
	FOREIGN KEY(ReservacionId) REFERENCES Reservacion(ReservacionId)
	)

	CREATE TRIGGER crearFactura
	ON Reservacion 
	FOR INSERT 
	AS 
	DECLARE @idUsuario INT
	DECLARE @fecha DATE 
	SELECT @idUsuario = UsuarioId FROM Reservacion WHERE ReservacionId = (SELECT MAX(ReservacionId) FROM Reservacion) 
	SELECT @fecha = GETDATE()
	INSERT INTO Factura(UsuarioId,fechaFacturacion)
	VALUES(@idUsuario,@fecha)
	

	CREATE TRIGGER crerDetalle
	ON Factura 
	FOR INSERT 
	AS 
	DECLARE @idFactura INT 
	DECLARE @idReservacion INT 
	SELECT @idFactura = FacturaId FROM Factura WHERE FacturaId = (SELECT MAX(FacturaId) FROM Factura)
	SELECT @idReservacion = ReservacionId FROM Reservacion WHERE ReservacionId = (SELECT MAX(ReservacionId) FROM Reservacion)
	INSERT INTO DetalleFactura(FacturaId,ReservacionId)
	VALUES (@idFactura,@idReservacion)
	PRINT('SE HA CREADO EL DETALLE');

	CREATE VIEW MayorSolicitud
	AS
	SELECT TOP 1000 COUNT(htl.HotelId) AS Cantidad_Reservas, htl.NombreHotel
	FROM (Hotel htl
		INNER JOIN Habitacion hbc ON  hbc.HabitacionId = htl.HotelId
		INNER JOIN Reservacion rvc ON hbc.HabitacionId = rvc.HabitacionId
		)
		GROUP BY NombreHotel 
		ORDER BY COUNT(htl.HotelId) ASC
	

	CREATE VIEW Reservaciones
	AS
	SELECT htl.NombreHotel, rvc.ReservacionId,rvc.Ckeck_in, rvc.Ckeck_out, us.correo  
	FROM Hotel htl
		INNER JOIN Habitacion hbc ON  hbc.HabitacionId = htl.HotelId
		INNER JOIN Reservacion rvc ON hbc.HabitacionId = rvc.HabitacionId
		INNER JOIN Usuario us ON us.UsuarioId = rvc.UsuarioId
		
	


	SELECT * FROM Usuario
	SELECT * FROM AdmonApp
	SELECT * FROM Municipio
	SELECT * FROM Departamento
	SELECT * FROM Hotel
	SELECT * FROM AdmonHotel
	SELECT * FROM CategoriaEvento
	SELECT * FROM Evento
	SELECT * FROM TipoHabitacion
	SELECT * FROM Habitacion
	SELECT * FROM Reservacion
	SELECT * FROM Factura
	SELECT * FROM DetalleFactura
	SELECT * FROM Reservaciones

	SELECT * FROM EstadoHabitacion
	INSERt INTO EstadoHabitacion(NombreEstadoHabitacion)
	VALUES('Diponible'),('Ocupada')

	SELECT * FROM Servicio
	INSERT INTO Servicio(NombreServicio,Descripcion,Precio,HotelId)
	VALUES('Wifi Gratis', 'Punto de acceso inalambrico en todo el hote las 24 horas.', 15.60, 1)

	INSERT INTO CategoriaEvento(NombreCategoriaEvento)
	VALUES('Cultural'),('Familiar'),('Conferencia'),('Boda'),('Concierto')

	INSERT INTO Evento(NombreEvento, Descripcion, Fecha,CategoriaEventoId, HotelId)
	VALUES('Cambio Climatica', 'La necesidad de que los guatemaltecos conozcan del cambio climatico no hace realizar esta conferencia', 12-12-17, 3, 1)

	INSERT INTO TipoHabitacion(NombreTipoHabitacion, Precio, Descripcion)
	VALUES ('Habitacion Triple', 300, 'Habitacion con tres camas para un maximo de 4 personas')
	
	INSERT INTO Habitacion(TipoHabitacionId, HotelId,EstadoHabitacionId)
	VALUES(1,2,1)

	INSERT INTO Reservacion(Ckeck_in,Ckeck_out,UsuarioId,HabitacionId)
	VALUES(2017-06-29,2017-08-1,1,2)

	INSERT INTO Usuario(NombreUsuario,ApellidoUsuario,correo,Password,Nit)
	VALUES ('Henry','Vasquez','henryvsquez42@gmail.com','Hacker42',23445566)

	INSERT INTO AdmonApp(NombreAdmonApp, Correo, Password)
	VALUES ('root','root@webhote.com','adminApp')

	INSERT INTO Municipio(NombreMunicipio)
	VALUES ('Villa Nueva'),('Mixco'),('Fraijanes')

	INSERT INTO Departamento(NombreDepartamento)
	VALUES ('Ciudad de Guatemala'),('Solola'),('Peten')

	INSERT INTO Hotel(NombreHotel, Direccion, DepartamentoId, MunicipioId)
	VALUES('Solei','Zona 10',1,1),('Camino Real','Zona 10',2,2)

	INSERT INTO AdmonHotel(NombreAdmonHotel, Correo, Password,HotelId)
	VALUES ('adminHotel','adminHotel@hotel.com','adminHotel',1)
	

	
	